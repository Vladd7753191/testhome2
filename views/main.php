<?php
    Views::getHeader();
    $users = $pageData["users"];
?>
<div class="container mt-5  " >
    <div class="row mt-5 justify-content-center">
        <div class="col-8 my-5 align-content-center">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th><a href="<?php echo sortUrl('id')?>">ID <?php echo sortArrowSign('id')?></a></th>
                    <th><a href="<?php echo sortUrl('name')?>">Name <?php echo sortArrowSign('name')?></a></th>
                    <th><a href="<?php echo sortUrl('email')?>">Email <?php echo sortArrowSign('email')?></a></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($users as $user): ?>
                    <tr>
                        <td><?php echo $user["id"]?></td>
                        <td><?php echo $user["name"]?></td>
                        <td><?php echo $user["email"]?></td>
                        <td> <a class="btn btn-info" href='/user/userPage?userId=<?php echo $user["id"]?>'>Подробнее</a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php Views::getFooter(); ?>