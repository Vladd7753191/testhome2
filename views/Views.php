<?php

class Views
{
    public function render($tpl, $pageData = [])
    {
        include VIEW_PATH.$tpl.'.php';
    }

    public static function getHeader()
    {
        require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "header.php");
    }

    public static function getFooter()
    {
        require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "footer.php");
    }
}