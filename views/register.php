<?php require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "header.php");?>
<div class="container mt-5">
    <div class="row justify-content-lg-center">
        <div class="col-8">
            <form class="form-horizontal mt-5" role="form" method="POST" id="registerForm" action="/user/register">
                <input type="hidden" name="csrfToken" value="">
                <h2 ><?php$pageData['title']?></h2>
                <?php if(!empty($pageData['error'])):?>
                    <p style="color:red;"><?php echo $pageData['error']; ?></p>
                <?php endif; ?>
                <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">Имя</label>
                    <div class="col-sm-9">
                        <input type="text" id="name" name="name" placeholder="Имя" class="form-control" value="" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="login" class="col-sm-3 control-label">Логин</label>
                    <div class="col-sm-9">
                        <input type="text" id="login" name="login" placeholder="Логин" class="form-control" value="" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email" class="col-sm-3 control-label">Email адрес</label>
                    <div class="col-sm-9">
                        <input type="text" id="email" name="email" placeholder="Email адрес" class="form-control" value="" autofocus>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-sm-3 control-label">Пароль</label>
                    <div class="col-sm-9">
                        <input type="password" id="password" name="password" placeholder="Пароль" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label for="captcha" class="col-sm-3 control-label">Проверочный код</label>
                    <div class="col-sm-9">
                        <img src="/captcha" onclick="reloadCaпшеptcha()" id="captchaImage">
                        <input type="text" id="captcha" name="captcha" placeholder="" class="form-control" value="">
                    </div>
                </div>
                <button type="submit" onclick=" // return false;" class="btn btn-primary btn-sml">Зарегистрироватся</button>
            </form>
        </div>
    </div>
</div>
<?php require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "footer.php") ?>