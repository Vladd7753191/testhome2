<?php require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "header.php");?>
<div class="container mt-5">
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card mt-5">
            <div class="card-header">Вход на страницу</div>

            <div class="card-body">
                <form method="POST" action="/user/login">
                    <?php if(!empty($pageData['error'])):?>
                    <p><?php echo $pageData['error']; ?></p>
                    <?php endif; ?>
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email адрес
                        </label>
                        <div class="col-md-6">
                            <input id="login" type="email" class="form-control "
                                   name="email" value="" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control " name="password" >
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">Войти</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<?php require_once(VIEW_PATH . "layout". DIRECTORY_SEPARATOR.  "footer.php") ?>