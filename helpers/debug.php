<?php

function prn($content)
{
    echo '<pre>';
    print_r($content);
    echo '</pre>';
}

function prd($content)
{
    prn($content);
    die();
}

function dump($content)
{
    echo '<pre>';
    var_dump($content);
    echo '</pre>';
}

function dd($content)
{
    dump($content);
    die();
}