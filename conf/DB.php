<?php


class DB
{
    const USER = "root";
    const PASS = "";
    const HOST = "vladislavtest";
    const DB = "register_users";

    private static $instance;

    private $dbConnection;

    /**
     * DB constructor.
     * @param $dbConnection
     */
    public function __construct()
    {
        $user = self::USER;
        $pass = self::PASS;
        $host = self::HOST;
        $db = self::DB;

        $this->dbConnection = new PDO("mysql:dbname=$db;host=$host", $user, $pass);
        $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function connToDB()
    {
        return self::getInstance()->getDbConnection();
    }

    /**
     * @return PDO
     */
    public function getDbConnection(): PDO
    {
        return $this->dbConnection;
    }
}