<?php

session_start();

define('ROOT', $_SERVER['DOCUMENT_ROOT']);
define('CONTROLLER_PATH', ROOT.DIRECTORY_SEPARATOR.'controllers'.DIRECTORY_SEPARATOR );
define('HELPERS_PATH',ROOT.DIRECTORY_SEPARATOR.'helpers'.DIRECTORY_SEPARATOR);
define('MODEL_PATH',ROOT.DIRECTORY_SEPARATOR.'models'.DIRECTORY_SEPARATOR);
define('VIEW_PATH',ROOT.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR);
define('SRC_PATH',ROOT.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR);
define('VALIDATOR_PATH',SRC_PATH.'Validator'.DIRECTORY_SEPARATOR);

require_once('DB.php');
require_once('Routing.php');
require_once (HELPERS_PATH.'debug.php');
require_once (HELPERS_PATH.'view.php');
require_once (MODEL_PATH.'Model.php');
require_once VIEW_PATH.'Views.php';
require_once CONTROLLER_PATH.'Controller.php';
require_once SRC_PATH.'Auth.php';

Routing::buildRoute();

//C:\xampp\htdocs\vladislavtest\models"\"Model.php'