<?php

class UserModel extends Model
{


    public function getUserByEmailAndPassword(string $email, string $password)
    {
        $password = md5($password);

        $sql = "SELECT * FROM users where email = :email AND password = :password  LIMIT 1";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":email", $email,PDO::PARAM_STR);
        $stmt->bindValue(":password", $password,PDO::PARAM_STR);
        $stmt->execute();

        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

    /**
     * @param string $name
     * @param string $login
     * @param string $email
     * @param string $password
     * @return int - ID созданного пользователя
     */
    public function register(string $name,string $login,string $email, string $password,string $apiKey): int
    {
        $password = md5($password);

        $sql = "INSERT INTO users (name, login, email, password,apiKey) VALUES (:name, :login, :email, :password, :apiKey)";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue("name", $name,PDO::PARAM_STR);
        $stmt->bindValue("login", $login,PDO::PARAM_STR);
        $stmt->bindValue("email", $email,PDO::PARAM_STR);
        $stmt->bindValue("password", $password,PDO::PARAM_STR);
        $stmt->bindValue("apiKey", $apiKey,PDO::PARAM_STR);
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    public function getUserById(int $id)
    {
        $sql = "SELECT * FROM users where id = :id LIMIT 1";

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(":id", $id,PDO::PARAM_INT);
        $stmt->execute();

        $res = $stmt->fetch(PDO::FETCH_ASSOC);

        return $res;
    }

    public function getAllUsers($sortField = null, $sortOrder = 'ASC')
    {
        $sql = "SELECT * FROM users";

        if ($sortField !== null) {
            $sql.= " ORDER BY `$sortField` $sortOrder";
        }

        $stmt = $this->db->prepare($sql);

        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $res;
    }
}