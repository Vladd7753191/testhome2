<?php

class Auth
{
    public $user;

    public function setUser($user)
    {
        $this->user = $user;
        $this->updateSession();
    }

    private function updateSession()
    {
        $_SESSION["userId"] = $this->user['id'];
    }
}