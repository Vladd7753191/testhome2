<?php
require_once MODEL_PATH.'UserModel.php';
class IndexController extends Controller
{
    private $userModel;
    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->checkAuth();
        parent::__construct();
    }

    public function index()
    {

        $this->pageData["users"]= $this->userModel->getAllUsers(
            $this->getParameter('sortField'),
            $this->getParameter('sortOrder')
        );
        $this->view->render('main', $this->pageData);
    }

    public function sessionOff(){
        session_destroy();
        header("Location:/");
    }
}