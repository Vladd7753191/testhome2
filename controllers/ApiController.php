<?php

require_once MODEL_PATH . 'UserModel.php';

class ApiController extends Controller
{
    public $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();
        $this->checkApiAuth();
        parent::__construct();
    }

    public function usersAll()
    {
        echo json_encode($this->userModel->getAllUsers());
    }
}