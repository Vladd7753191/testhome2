<?php
require_once MODEL_PATH.'UserModel.php';

class UserController extends Controller
{
    /**
     * @var UserModel
     */

    private $userModel;

    public function __construct()
    {
        $this->userModel = new UserModel();

        parent::__construct();
    }

    public function loginForm()
    {
        $this->pageData['title'] = 'Вход';

        $this->view->render('login', $this->pageData);
    }

    public function userPage(){
        $this->pageData['userId'] = $this->getParameter('userId');
        $this->pageData['title'] = 'Страничка пользователя';
        $this->view->render('userPage',$this->pageData);
    }

    public function login()
    {
        $email = $this->getPostParameter('email');
        $password = $this->getPostParameter('password');

        $user = $this->userModel->getUserByEmailAndPassword($email, $password);

        if (!empty($user)) {
            $auth = new Auth();
            $auth->setUser($user);

            $this->redirectAfterAuth();
        }

        $this->pageData['error'] = "Логин не удался";
        $this->loginForm();
    }

    public function registerForm()
    {
        $this->pageData['title'] = 'Регистрация';

        //генерируется и записывается в сессию какой-то токен.

        $this->view->render('register', $this->pageData);
    }

    public function register()
    {
        if ($this->isAjax()) {
            echo json_encode(['privet' => 'backend']);
            die();
        }

        require_once VALIDATOR_PATH.'RegisterValidator.php';

        $validator = new RegisterValidator($_POST);

        $name = $this->getPostParameter('name');
        $login = $this->getPostParameter('login');
        $email = $this->getPostParameter('email');
        $password = $this->getPostParameter('password');
        $apiKey = md5( time().rand(0,9));


        if ($validator->validate()) {
            //Регистрируем пользователя
            $userId = $this->userModel->register($name, $login, $email, $password,$apiKey);

            $user = $this->userModel->getUserById($userId);

            $auth = new Auth();
            $auth->setUser($user);

            $this->redirectAfterAuth();
        }

        $this->pageData['error'] = implode(', ', $validator->getErrors());
        $this->registerForm();
    }

    private function redirectAfterAuth()
    {
        header("Location:/");
    }
}